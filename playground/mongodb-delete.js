const {MongoClient , ObjectID }= require('mongodb');


//object id generator
// var obj = new ObjectID();
//
// console.log(obj);

// var user = {name:'Abhinish', age:25};
// var {name} = user;
// console.log(name);

MongoClient.connect('mongodb://localhost:27017/TodoApp', (err,db) => {
    if(err){
      return console.log('Unable to connect to MongoDB server');
    }
    console.log('Connected to MongoDB server');
    // db.collection('Todos').insertOne({
    //     text:'Something to do',
    //     completed: false
    // },(err,result) => {
    //     if(err){
    //         return console.log('Unable to insert todo', err);
    //     }
    //
    //     console.log(JSON.stringify(result.ops,undefined,2));
    // });

    //insert new doc into Users (name,age, location)

    // db.collection('Users').insertOne({
    //     name:'Abhinish Raj',
    //     age:23,
    //     location:'Bangalore'
    // },(err,result) => {
    //     if(err){
    //         return console.log('Unable to insert todo', err);
    //     }
    //
    //     console.log(JSON.stringify(result.ops,undefined,2));
    //
    //     console.log(result.ops[0]._id.getTimestamp());
    //

    // });

    // db.collection('Todos').find({completed: false}).toArray().then((docs) => {
    // console.log('Todos');
    // console.log(JSON.stringify(docs,undefined,2));
    // }, (err) => {
    //     console.log('Unable to fetch the todos',err);
    // });

    // db.collection('Todos').find({
    //     _id: new ObjectID('5a155d32652a521a9ca7a3e3')}).toArray().then((docs) => {
    //     console.log('Todos');
    //     console.log(JSON.stringify(docs,undefined,2));
    // }, (err) => {
    //     console.log('Unable to fetch the todos',err);
    // });
    // db.collection('Todos').find().count().then((count) => {
    //     console.log(`Todos count: ${count}`);
    //     // console.log(JSON.stringify(docs,undefined,2));
    // }, (err) => {
    //     console.log('Unable to fetch the todos',err);
    // });

    // db.collection('Users').find({name:'Abhinish Raj'}).count().then((count) => {
    //     console.log(`Todos count: ${count}`);
    //     // console.log(JSON.stringify(docs,undefined,2));
    // }, (err) => {
    //     console.log('Unable to fetch the todos',err);
    // });
    // db.collection('Users').find({age:23}).count().then((count) => {
    //     console.log(`Todos count: ${count}`);
    //     // console.log(JSON.stringify(docs,undefined,2));
    // }, (err) => {
    //     console.log('Unable to fetch the todos',err);
    // });
    //
    // db.collection('Users').find({location:'Bangalore'}).count().then((count) => {
    //     console.log(`Todos count: ${count}`);
    //     // console.log(JSON.stringify(docs,undefined,2));
    // }, (err) => {
    //     console.log('Unable to fetch the todos',err);
    // });

    // db.collection('Todos').deleteMany({text:'Eat dinner'}).then((result) => console.log(result));

    // db.collection('Todos').deleteOne({text:'Walk the dog'}).then((result) => console.log(result));

    // db.collection('Todos').findOneAndDelete({text:'Walk the dog'}).then((result) => console.log(result));

    // db.collection('Users').deleteMany({name:'Abhinish Raj'}).then((result) => console.log(result));

    // db.collection('Users').findOneAndDelete({name:'Abhinish Raj11'}).then((result) => console.log(result));

    // db.collection('Users').findOneAndDelete({
    //     _id: new ObjectID('5a155806ceb03a2de01936bd')}).then((result) => console.log(result));

    db.collection('Users').findOneAndDelete({
        _id: new ObjectID('5a155538f677b23504d269a2')}).then((result) => console.log(JSON.stringify(result,undefined,2)));
    // db.close();
});